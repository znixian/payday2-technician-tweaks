
function SentryGunInteractionExt:_on_death_event()
	-- Do nothing
end

local old_timer_value = BaseInteractionExt._timer_value
function SentryGunInteractionExt:_timer_value()
	if self._unit:damage()._dead then
		return 10 -- TODO it was going to be 20, but that may be too long
	end

	return old_timer_value(self)
end
